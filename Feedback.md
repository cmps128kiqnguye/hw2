

#### Timestamp  
4/26/2016 23:29:13  


#### Graded score  
15/15  


#### Tests passed  
15/17  


#### Username  
kiqnguye@ucsc.edu  


#### Commit URL  
https://bitbucket.org/cmps128kiqnguye/hw2/overview  


#### UCSC IDs of team members  
blambrig  
kiqnguye  


#### Feedback1  
Tests that didn't pass:   
put key without value  
put key that's too long  


#### Feedback2  
N/A  
