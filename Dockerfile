FROM ubuntu:latest

MAINTAINER Maintaner kiqnguye

RUN echo "deb http://archive.ubuntu.com/ubuntu/ $(lsb_release -sc) main universe" >> /etc/apt/sources.list

RUN apt-get update

RUN apt-get -y install curl tar git wget

RUN apt-get -y install python python-dev python-distribute python-pip

RUN mkdir app
COPY . /app
RUN pip install -r /app/requirements.txt

EXPOSE 8080

CMD ["python", "/app/hello.py", "-d"]

