from flask import Flask, request, Response, json
import os

app = Flask(__name__)

@app.route('/')
def root(): pass

@app.route('/hello', methods=['GET'])
def hello():
    return 'Hello World!'
	
@app.route('/kvs/<key>', methods=['PUT', 'GET', 'DELETE'])
def kvs(key):
	if request.method == 'PUT':
		val = request.form['val']
		try:
			f = open(key, 'r+')
			print("Opening file " + key + "\nUpdating Value")
			f.write(val)
			f.close()
			data = {'replaced': 1, 'msg': 'success'}
			jsData = json.dumps(data)
			resp = Response(jsData, status=200, mimetype='application/json')
			return resp
		except OSError:
			print("File not found. Creating new file...")
			f = open(key, 'w')
			f.write(val)
			f.close()
			data = {'replaced': 0, 'msg': 'success'}
			jsData = json.dumps(data)
			resp = Response(jsData, status=201, mimetype='application/json')
			return resp
	if request.method == 'GET':
		try:
			f=open(key,'r')
			value = f.read()
			f.close()
			data = {'value': value, 'msg': 'success'}
			jsData = json.dumps(data)
			resp = Response(jsData, status=200, mimetype='application/json')
			return resp
		except OSError:
			print("Error: File not Found")
			data = {'msg': 'error', 'error': 'key does not exist'}
			jsData = json.dumps(data)
			resp = Response(jsData, status=404, mimetype='application/json')
			return resp
	if request.method == 'DELETE':
		try:
			os.remove(key)
			print("File " + key + " deleted")
			data = {'msg': 'success'}
			jsData = json.dumps(data)
			resp = Response(jsData, status=200, mimetype='application/json')
			return resp
		except:
			print("Error: File not Found")
			data = {'msg': 'error', 'error': 'key does not exist'}
			jsData = json.dumps(data)
			resp = Response(jsData, status=404, mimetype='application/json')
			return resp

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8080')